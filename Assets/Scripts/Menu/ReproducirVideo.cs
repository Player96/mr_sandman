﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ReproducirVideo : MonoBehaviour {
	MovieTexture cinematica;
	// Use this for initialization
	void Start () {
		cinematica = ((MovieTexture)GetComponent<Renderer>().material.mainTexture);
		GetComponent<AudioSource>().clip = cinematica.audioClip;
		cinematica.Play();
		GetComponent<AudioSource>().Play();
		}
	
	// Update is called once per frame
	void Update () {

		if (!cinematica.isPlaying || Input.GetKeyDown (KeyCode.Return)) {
			SceneManager.LoadScene ("Cargando");
		}
	}
}
