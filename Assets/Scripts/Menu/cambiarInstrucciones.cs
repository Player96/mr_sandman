﻿using UnityEngine;
using System.Collections;

public class cambiarInstrucciones : MonoBehaviour {
    public Canvas canvas1;
    public Canvas canvas2;
 
	// Use this for initialization
	void Start () {

        canvas1.enabled = true;
        canvas2.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void rollear()
    {
        if (canvas1.enabled == true)
        {
            canvas1.enabled = false;
            canvas2.enabled = true;
        }
  
    }
}
