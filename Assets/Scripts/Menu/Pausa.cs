﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Pausa : MonoBehaviour {
    public Canvas Pause;

    bool pausa = false;
    // Use this for initialization
    // Update is called once per frame

        void Start()
    {
        Pause.enabled = false;
    }

	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape) && pausa == false)
        {
            pausa = true;
            Time.timeScale = 0;
            Pause.enabled = true;
        }
       
        
       else if(Input.GetKeyDown(KeyCode.Escape)&&pausa == true)
        {
            pausa = false;
            Time.timeScale = 1;
            Pause.enabled = false;
        }
       
	}

    public void Continuar()
    {
        pausa = false;
        Time.timeScale = 1;
        Pause.enabled = false;
    }
    public void Guardar()
    {
        //guardar :v
    }

    public void Salir()
    {
        SceneManager.LoadScene("menu");
    }
}
