﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Cargando : MonoBehaviour {

	private float tiempo;

	// Use this for initialization
	void Start () {
		SceneManager.LoadSceneAsync ("Nivel1.0");
	}
	
	// Update is called once per frame
	void Update () {
		tiempo += 1 * Time.deltaTime;
		if (tiempo >= 10) {
			SceneManager.LoadScene ("Nivel1.0");
		}
	}
}
