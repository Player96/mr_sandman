﻿using UnityEngine;
using System.Collections;

public class UpDown : MonoBehaviour
{


    public float CaminaArriba;
    public float CaminaAbajo;
    public float velocidad;
    private bool camina = true;

    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(0, 0, 0);
        if (camina == true)
        {
            gameObject.transform.Translate(0, -velocidad * Time.deltaTime, 0);
            if (gameObject.transform.position.y <= CaminaAbajo)
            {
                camina = false;
            }
        }
        if (camina == false)
        {
            gameObject.transform.Translate(0, velocidad * Time.deltaTime, 0);
            if (gameObject.transform.position.y >= CaminaArriba)
            {
                camina = true;
            }
        }
    }
}
