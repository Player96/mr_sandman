﻿using UnityEngine;
using System.Collections;

public class PParent : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionStay2D(Collision2D other)
    {
        // Incluímos como hijo de la plataforma a cualquier objeto que se pose sobre ella
        other.transform.parent = transform;
    }

    void OnCollisionExit2D(Collision2D other)
    {
        // Excluímos como hijo de la plataforma a cualquier objeto que se separe de ella
        other.transform.parent = null;
    }
}
