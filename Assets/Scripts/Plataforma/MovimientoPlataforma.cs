﻿using UnityEngine;
using System.Collections;

public class MovimientoPlataforma : MonoBehaviour {

	public Vector3 velocidad;

	public Vector3 distanciaEntrePlataformas;

	// Update is called once per frame
	void Update () {
		moverKits ();	
	}

	void moverKits(){

		this.transform.position = this.transform.position + (velocidad * Time.deltaTime);

		if (this.transform.position.y >= 12f) {

			this.transform.position -= distanciaEntrePlataformas;
		}

	}
}
