﻿using UnityEngine;
using System.Collections;

public class Dtorreta : MonoBehaviour {
    public GameObject bala;
    public int Direccion;
    public float speed;
    public float delay;
    private bool disparo;


    void start()
    {
        

  
    }
    // Update is called once per frame
	void Update()
	{
		delay += 1 * Time.deltaTime;
	}
    void FixedUpdate()
    {
		if (disparo && delay>=1.2) {
			GameObject newBala = Instantiate(bala, transform.position, transform.rotation) as GameObject;
			newBala.GetComponent<Rigidbody2D>().velocity = transform.right * speed * Direccion;
			delay = 0;
			staticimplements.disparar = true;
		}
    }
    void OnTriggerStay2D(Collider2D c)
    {
    	if (c.CompareTag("Personaje"))
        {
			disparo = true;
    	}
    }
    void OnTriggerExit2D(Collider2D c)
    {
        staticimplements.disparar = false;
		disparo = false;
    }

}