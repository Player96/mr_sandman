﻿using UnityEngine;
using System.Collections;

public class EnemyMoveFantasma : MonoBehaviour
{

    public float CaminaArriba;
    public float CaminaAbajo;
    public float velocidad;
    private bool camina = true;
	public AudioSource muerte;
	public ParticleSystem muerteSandman;

    void Start()
    {       
        
    }
	// Update is called once per frame
    void Update()
    {       
        gameObject.transform.Translate(0, 0, 0);   
		if (camina ==true )
        {
        	gameObject.transform.Translate(0, -velocidad  * Time.deltaTime, 0);          
            if (gameObject.transform.position.y <= CaminaAbajo)
            {
            	camina = false;
            }
        }
        if (camina ==false )
        {
            gameObject.transform.Translate(0, velocidad  * Time.deltaTime, 0);      
            if (gameObject.transform.position.y >= CaminaArriba)
            {
            	camina = true;
            }
        }                      
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Personaje")
        {
			ParticleSystem newParticle = Instantiate (muerteSandman, coll.transform.position, Quaternion.identity) as ParticleSystem;
			newParticle.Play ();
			muerte.Play ();
            Destroy(coll.gameObject);
        }
    }
}