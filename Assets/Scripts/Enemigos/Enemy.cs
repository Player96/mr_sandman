﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {


    public float velocity;
    public Transform ground;
    public LayerMask layerMask;
  
    private Rigidbody2D enemigo;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, GetComponent<Rigidbody2D>().velocity.y);

        bool groundCheck = Physics2D.Linecast( ground.position, (Vector2)ground.position  + Vector2.down);
       
        if (!groundCheck)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            velocity *= -1;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {

		if (col.gameObject.CompareTag ( "wall"))
        {
			transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
			velocity *= -1;
        }
    }
}
