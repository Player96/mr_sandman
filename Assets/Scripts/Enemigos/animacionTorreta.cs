﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class animacionTorreta : MonoBehaviour {

    private Animator anim;
    private bool shoot;

	public AudioClip[] sonidos;
	public AudioMixerGroup output;


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        anim.enabled = false;
       
	}
	
	// Update is called once per frame
	void Update () {
        if (staticimplements.disparar)
        {
            PlaySound();
            anim.enabled = true;
        }
        else { anim.enabled = false; }
	}
	void PlaySound(){

		int randomNumber = Random.Range (0, sonidos.Length);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
}
