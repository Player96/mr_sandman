﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class CambioNivel : MonoBehaviour
{

	public AudioClip[] sonidos;
	public AudioMixerGroup output;

	public AudioSource _Audio1;
	public AudioSource _Audio2;



    // Use this for initialization
    void Start()
    {
		_Audio1.Play ();
	}

    // Update is called once per frame
    void Update()
	{

        if (GameController.vivo==false )
        {
           
            muerte();
            GameController.vivo = true;
        }
            if (Input.GetKeyDown (KeyCode.C)) {
			GetComponent<Transform> ().position = new Vector3 (-31.5f, -57.3f, GetComponent<Transform> ().position.z);
			Camera.main.GetComponent<Transform> ().position = new Vector3 (-31.5f, -57.3f, Camera.main.GetComponent<Transform> ().position.z);

			if (_Audio1.isPlaying) {
				_Audio1.Stop ();
				_Audio2.Play ();
             
			}
			PlaySound ();
		}
		if (Input.GetKeyDown (KeyCode.V)) {
			GetComponent<Transform> ().position = new Vector3 (-36.1f, 8.1f, GetComponent<Transform> ().position.z);
			Camera.main.GetComponent<Transform> ().position = new Vector3 (-36.1f, 8.1f, Camera.main.GetComponent<Transform> ().position.z);
			if (_Audio2.isPlaying) {
				_Audio1.Play ();
				_Audio2.Stop ();

			}
			PlaySound ();
		}
	}

    
	void PlaySound(){
	
		int randomNumber = Random.Range (0, sonidos.Length);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
	public void muerte(){
		if (_Audio2.isPlaying) {
			_Audio1.Play ();
			_Audio2.Stop ();
		}
	}
}