﻿using UnityEngine;
using System.Collections;

public class DestruirEnemigo : MonoBehaviour {

	public ParticleSystem muerteEnemigo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D enemy)
    {
        if (enemy.CompareTag("Enemigo"))
        {

			ParticleSystem newParticle = Instantiate (muerteEnemigo, enemy.transform.position, Quaternion.identity) as ParticleSystem;
			newParticle.Play ();

            Destroy(enemy.gameObject);
		}
	}
}