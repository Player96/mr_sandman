﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class PLayermelee : MonoBehaviour {

	private bool atack =false;
    private float timer = 0;
    public float CD = 0.5f;
	public ParticleSystem muerteEnemigo;
    

    public Collider2D trigger;

	public AudioClip[] sonidos;
	public AudioMixerGroup output;


    void Start()
    {

        trigger.enabled = false;
    }
    void Update()
    {
        trigger.enabled = false;
        if (Input.GetKeyDown(KeyCode.L) && !atack)
        {
            atack = true;
            timer = CD;
           trigger.enabled = true;


        }
        if (atack)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                atack = false;
                trigger.enabled = false;
         
            }
        }
    }
    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("Enemigo"))
        {
            PlaySound();
            ParticleSystem newParticle = Instantiate (muerteEnemigo, c.transform.position, Quaternion.identity) as ParticleSystem;
			newParticle.Play ();
			Destroy(c.gameObject);
        }
    }
	void PlaySound(){

		int randomNumber = Random.Range (0, sonidos.Length);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
}
