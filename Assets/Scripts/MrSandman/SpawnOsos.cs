﻿using UnityEngine;
using System.Collections;

public class SpawnOsos : MonoBehaviour {

	public GameObject prefab;
	public float numEnemigos;
	public float minX = 12f;
	public float maxX = 21f;
	public float minY = -1f;
	public float maxY = 3f;

	// Use this for initialization
	void Start () {

		GameObject newParent = GameObject.Find ("Enemigos");

		for(int i = 0;i<numEnemigos;i++){

			Vector3 newPos = new Vector3 (Random.Range(minX,maxX), Random.Range(minY,maxY),0);
			GameObject oso = Instantiate (prefab, newPos, Quaternion.identity) as GameObject;
			oso.transform.parent = newParent.transform;

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
