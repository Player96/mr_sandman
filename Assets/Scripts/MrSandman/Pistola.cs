﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Pistola : MonoBehaviour {
   
    public GameObject balaPrefab;
    public float speed;

	public AudioClip[] sonidos;
	public AudioMixerGroup output;


    void start()
    {
	}
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.K)) {
            GameObject newBala = Instantiate(balaPrefab, transform.position, transform.rotation) as GameObject;
             newBala.GetComponent<Rigidbody2D>().velocity = transform.right * speed;

			//Sonido Disparo
			PlaySound();
		}
	}

	void PlaySound(){

		int randomNumber = Random.Range (0, sonidos.Length);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
}