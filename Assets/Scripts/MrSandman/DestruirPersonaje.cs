﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class DestruirPersonaje : MonoBehaviour {

	public ParticleSystem muerteSandman;
    public AudioClip[] sonidos;
    public AudioMixerGroup output;


    public void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == ("Personaje"))
        {
            
         
            Destroy(c.gameObject);

            ParticleSystem newParticle = Instantiate(muerteSandman, c.transform.position, Quaternion.identity) as ParticleSystem;
            newParticle.Play();
            PlaySound();
            Destroy(newParticle.gameObject, 2f);
			GetComponent<CambioNivel> ().muerte ();
      
        }

    }
    void PlaySound()
    {

        int randomNumber = Random.Range(0, sonidos.Length);
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = sonidos[randomNumber];
        source.outputAudioMixerGroup = output;
        source.Play();
        Destroy(source, sonidos[randomNumber].length);
    }
}
