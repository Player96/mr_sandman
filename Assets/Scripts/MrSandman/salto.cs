﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class salto : MonoBehaviour {

    private Rigidbody2D cuerpo;
 
    public Transform ground;
    public float saltar;
    public LayerMask layerMask;

	public AudioClip[] sonidos;
	public AudioMixerGroup output;

    void Start()
    {
 
        cuerpo = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
       
        Vector2 offset = new Vector2(0, 0.1f);
        bool groundCheck = Physics2D.Linecast(ground.position, (Vector2)ground.position - offset,layerMask);

        if (groundCheck && (Input.GetKeyDown(KeyCode.Space)  ))
        {
  
   
            cuerpo.velocity = new Vector2(cuerpo.velocity.x, saltar);
			PlaySound ();
           


        }
    }

	void PlaySound(){

		int randomNumber = Random.Range (0, sonidos.Length -1);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
}
   

