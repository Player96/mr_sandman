﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovimientoMrSandman : MonoBehaviour
{

  //  private Vector2 movimiento;
    public float velocidad;
    bool giroizq = false;
    bool giroder = false;
    private int Ngiros;
    private Rigidbody2D cuerpo;
    private Animator anim;



    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetInteger("estado", 0);
        cuerpo = GetComponent<Rigidbody2D>();
       
        
    }

    void Update()
    {
     

        if (Input.GetKey(KeyCode.D))
        {

            giroizq = true;
            Flip();
            Ngiros -= 1;
           
            cuerpo.velocity = new Vector2(velocidad, cuerpo.velocity.y);
            anim.SetInteger("estado", 1);


            if (Input.GetKey(KeyCode.LeftShift))
            {
                cuerpo.velocity = new Vector2(cuerpo.velocity.x * 2, cuerpo.velocity.y);
            }


            giroder = false;
            if (Input.GetKey(KeyCode.Space))
            {
                anim.SetInteger("estado", 2);
            }
         

        }        else { anim.SetInteger("estado", 0); }
      

        if (Input.GetKey(KeyCode.A))
        {                     
            giroder = true;
            Flip();
            Ngiros += 1;
            cuerpo.velocity = new Vector2(-velocidad, cuerpo.velocity.y);
            anim.SetInteger("estado", 1);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                cuerpo.velocity = new Vector2(cuerpo.velocity.x * 2, cuerpo.velocity.y);
            }

            if (Input.GetKey(KeyCode.Space))
            {
                anim.SetInteger("estado", 2);
            }
       
            giroizq = false;
        }
        
              
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetInteger("estado", 2);
        }
        if (Input.GetKey(KeyCode.K) && GameController.Score >= 80)
        {
            anim.SetInteger("estado", 3 ) ;
        }
        if (Input.GetKey(KeyCode.L) && GameController.Score>=20)
        {
            anim.SetInteger("estado", 4);
        }



    }


    void Flip()
    {

        if (giroizq == false && Ngiros <= 0)
        {

            cuerpo.transform.Rotate(Vector3.up, 180);
            Ngiros = 1;
        }
        if (giroder == false && Ngiros >= 1)
        {

            cuerpo.transform.Rotate(Vector3.up, 180);
            Ngiros = 0;
        }
    }
}

