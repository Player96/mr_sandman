﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class Respawn : MonoBehaviour {
	
 	public  GameObject respawn;
	public ParticleSystem check;
	public float numero;

	bool colision=false;

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update() {
		if (!GameObject.Find("personaje")&& colision && GameController.checkpoints==numero)
        {
            
            GameObject uno = Instantiate(respawn, transform.position, transform.rotation) as GameObject;
            uno.name = "personaje";
            GameController.vivo = false;

            Camera.main.GetComponent<FollowTarget>().target = uno.transform;
        } 
	}

	void OnTriggerStay2D(Collider2D col){
		if (col.gameObject.name == "personaje") {
			GetComponent<MeshRenderer> ().enabled = true;
			if (GameController.checkpoints <= numero) {
				GameController.checkpoints = numero;
				colision = true;
			}


		}
	}
}
