﻿using UnityEngine;	
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class destroychakra1 : MonoBehaviour {

	public AudioClip[] sonidos;
	public AudioMixerGroup output;
	public ParticleSystem ParticulaRecoger;
    public AudioClip[] desbloquear;
    public AudioMixerGroup output2;

    // Use this for initialization
    void Start () {
   
        //recoger = GetComponent<ParticleSystem> ();
    }
	
	// Update is called once per frame
	void Update () {
        //if(score_T != null)
        if (GameController.Score >= 20)
        {
            GetComponent<PLayermelee>().enabled = true;
        }
        if (GameController.Score > 49)
        {
            GetComponent<salto>().enabled = false;
            GetComponent<DobleSalto>().enabled = true;
        }
        if (GameController.Score >= 80)
        {
            GetComponent<Pistola>().enabled = true;
        }


    }

    void OnTriggerEnter2D(Collider2D c)
    {

        if(c.CompareTag("ParticulasA"))
        {
            Destroy(c.gameObject);
        }

        if (c.CompareTag("Sand"))
        {
            GameController.Score++;
            ParticleSystem newParticle = Instantiate(ParticulaRecoger, c.transform.position, Quaternion.identity) as ParticleSystem;
            newParticle.Play();
            PlaySound();
            Destroy(c.gameObject);
            Destroy(newParticle.gameObject, 0.5f);

     
       
			
			if (GameController.Score == 20 || GameController.Score == 50 || GameController.Score == 80) {

                int randomNumber = Random.Range(0, sonidos.Length);
                AudioSource source = gameObject.AddComponent<AudioSource>();
                source.clip = desbloquear[randomNumber];
                source.outputAudioMixerGroup = output2;
                source.Play();
                Destroy(source, desbloquear[randomNumber].length);
            }
        }
    }


	void PlaySound(){

		int randomNumber = Random.Range (0, sonidos.Length);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
   
}
