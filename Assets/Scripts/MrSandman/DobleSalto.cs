﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class DobleSalto : MonoBehaviour
{

    private Rigidbody2D cuerpo;

    public float saltar;
    private int Nsaltos = 0;

	public AudioClip[] sonidos;
	public AudioMixerGroup output;

    // Use this for initialization
    void Start()
    {
        cuerpo = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            Nsaltos += 1;
            if (Nsaltos == 1 || Nsaltos == 2)
            {
                cuerpo.velocity = new Vector2(cuerpo.velocity.x, saltar);
            }
        }
    }
    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Plataforma" || c.gameObject.tag == "PDestroy")
        {
            Nsaltos = 0;
        }
    }
	void PlaySound(){
	
		int randomNumber = Random.Range (0, sonidos.Length -1);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);

	}
}
