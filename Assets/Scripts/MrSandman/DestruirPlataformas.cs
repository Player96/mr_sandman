﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class DestruirPlataformas : MonoBehaviour {

	public AudioClip[] sonidos;
	public AudioMixerGroup output;
    void Start()
    {
        enabled = false;
    }
    void Update()
    {
      
        }

    void OnCollisionEnter2D(Collision2D c) //Saltar y presionar S para destruir
    {
        if (Input.GetKey(KeyCode.S))
        {

            if (c.gameObject.tag == "PDestroy")
            {
				PlaySound ();
                Destroy(c.gameObject);
                Debug.Log("faiil");
            }
        } 
    }
	void PlaySound(){

		int randomNumber = Random.Range (0, sonidos.Length);
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		source.clip = sonidos [randomNumber];
		source.outputAudioMixerGroup = output;
		source.Play ();
		Destroy (source, sonidos [randomNumber].length);
	}
}
