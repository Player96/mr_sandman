﻿using UnityEngine;
using System.Collections;

public class moverizq : MonoBehaviour {

    private Rigidbody2D prop;
    public float velocidad=-1f;
    // Use this for initialization
    void Start () {
  
        prop = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        prop.velocity = new Vector2(velocidad, prop.velocity.y);

		Destroy (gameObject, 10f);

	}
}
