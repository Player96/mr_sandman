﻿using UnityEngine;
using System.Collections;

public class generador : MonoBehaviour
{
    public GameObject[] obj;
    private float tiempoMin = 6f;
    private float tiempoMax = 7f;

    // Use this for initialization
    void Start()
    {
        generar();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void generar()
    {
        Instantiate(obj[Random.Range(0, obj.Length)], transform.position, Quaternion.identity);
        Invoke("generar", Random.Range(tiempoMin, tiempoMax));
    }
    
}
