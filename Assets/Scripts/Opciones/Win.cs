﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

    }
	
	// Update is called once per frame
	void Update () {
        if (GameController.Score >= 150) { 
        GetComponent<Collider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
            }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("Personaje") && GameController.Score>=150)
        {
            SceneManager.LoadScene("Win");
        }
    }
}
