﻿using UnityEngine;
using System.Collections;

public class brillo : MonoBehaviour {

	public float alpha = staticimplements.brillo;

	// Use this for initialization
	void Start () {

		alpha= staticimplements.brillo;

	}
	
	// Update is called once per frame
	void Update () {
	
		GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, alpha);

	}

	public void cambiar(float newAlpha) {

		alpha = newAlpha;
		staticimplements.brillo=newAlpha;

	}
}
