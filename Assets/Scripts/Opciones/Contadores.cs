﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class Contadores : MonoBehaviour {
    public Text texto_Score;
    public Text texto_Time;
    public float n = 120;
    public Text melee;
    public Text Dsalto;
    public Text disparo;
    public Image smelee;
    public Image ssalto;
    public Image sdisparo;

    // Use this for initialization
    void Start () {
        melee.enabled = false;
        smelee.enabled = false;
        Dsalto.enabled = false;
        ssalto.enabled = false;
        disparo.enabled = false;
        sdisparo.enabled = false;
        GameController.Score = 0;
       
        GameController.checkpoints = 0;


    }

    // Update is called once per frame
    void Update() {
        if (GameController.Score >= 20) //melee
        {
            melee.enabled = true;
            smelee.enabled = true;
        }
        if (GameController.Score >= 50) // doble salto
        {
            Dsalto.enabled = true;
            ssalto.enabled = true;
        }
        if (GameController.Score >= 80) // disparo
        {
            disparo.enabled = true;
            sdisparo.enabled = true;
        }

        texto_Score.text = GameController.Score.ToString();
        texto_Time.text = GameController.timee.ToString();

        GameController.timee = n - 1 * Time.deltaTime;
        n = GameController.timee;



        //  @"C:\Users\Public\TestFolder\WriteLines2.txt"
        if (n < 0)
        {
           
            SceneManager.LoadScene("GameOver");
        }
        // File.WriteAllText("C:/ Vidas.txt",GameController.vidas.ToString());
    }
}
